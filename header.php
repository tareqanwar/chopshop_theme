<?php
/**
 * The template for displaying the header
 * 
 * @package chopshop
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css" />
	<link rel="stylesheet" type="text/css" href="<?=get_stylesheet_uri()?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<nav class="navbar has-shadow is-spaced" style="transition: all 0.3s ease 0s;">
    <div class="container">
        <div class="navbar-brand">
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>

			<a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
			</a>
        </div>
        <div id="navMenuIndex" class="navbar-menu">
			<div class="navbar-start">
				<!-- navbar items -->
				<?php if ( has_nav_menu( 'primary' ) ) : ?>
					<?php
						wp_nav_menu( array(
							'theme_location' => 'primary',
							'menu_class'     => 'primary-menu',
						) );
					?>
				<?php endif; ?>
			</div>

			<div class="navbar-end">
				<!-- navbar items -->
				<?php get_search_form(); ?>
			</div>
        </div>
    </div>
</nav>

<main>
