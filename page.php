<?php get_header(); ?>

	<div class="container page">

		<div class="main">

			<?php while(have_posts()) : ?>
	
				<?php the_post(); ?>

				<?php the_title('<h1>','</h1>'); ?>

				<?php the_content('<div>', '</div>'); ?>

			<?php endwhile; ?>

		</div><!-- /main -->

		<?php get_sidebar('page'); ?>

</div><!-- /container -->

<?php get_footer(); ?>