<?php
get_header(); ?>

<section class="section">
    <div class="container">
        <div class="columns is-mobile is-multiline">
            <div class="column is-12-mobile is-12-tablet is-9-desktop">
                <?php while(have_posts()) : ?>
        
                    <?php the_post(); ?>

                    <div class="columns is-mobile is-multiline">
                        <div class="column is-12">
                            <a href="<?php the_permalink() ?>"><?php the_title('<h2 class="title">','</h2>'); ?></a>
                        
                            <p class="subtitle is-4"><small>Posted on <?php the_date(); ?> at <?php the_time() ?> by <?php the_author() ?></small></p>
                            
                            <?php the_excerpt('<div class="content">', '</div>'); ?>
                                
                            <small><a href="<?php the_permalink(); ?>">Read more...</a></small>
                        </div>
                    </div>

                <?php endwhile; ?>
            </div>

            <div class="column is-3-desktop is-hidden-touch">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>
