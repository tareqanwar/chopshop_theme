<aside class="menu box">
    <!--Sidebar -->
    <p class="menu-label">Menu</p>

	<?php wp_nav_menu() ?>

    <p class="menu-label">Archives</p>

    <?php wp_get_archives() ?>

    <p class="menu-label">Categories</p>

    <?php wp_list_categories(['title_li' => '']) ?>
</div>