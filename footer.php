<?php
/**
 * The template for displaying the footer
 *
 * @package chopshop
 */
?>
		</main>
		
		<footer class="footer">
			<div class="container">
			</div>
			<div class="content has-text-centered">
				<p>Copyright © <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			</div>
			<!-- /.container -->
		</footer><!-- /footer -->

	<!-- Bootstrap core JavaScript -->
	<script
		src="https://code.jquery.com/jquery-3.1.1.min.js"
		integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
		crossorigin="anonymous"></script>

<?php wp_footer(); ?>
</body>
</html>

